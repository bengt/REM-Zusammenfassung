# Requirements Engineering und Management

#### Wintersemester 2012-2013

## Kapitel 1: Grundlagen

### Motivation

Eine umfassende Anforderungsanalyse schafft eine stabile Basis für eine Erfolgreiche Systementwicklung, während die schlechte und unzureichende Anforderungen das Projekt gefärden. Die Anforderungen genau so wie die Anorerungsdokumente unterliegen bestimmten Qualitätskriterien. Klassifizieren der Anforderungen hilf die jüristischen Verbindlichkeiten fetzulegen und achzuvollziehen.

Mehrere Studien belegen, dass die Hälfte aller Softwarefehler ihren Uhrsprung in fehlerhaften Anforderungen hat. Man behauptet, dass nur 54% der ursprünglisch definierten Funktionalität in Projkten ausgeliefert und dass 45% der ausgelieferten Funktinalität nie benutzt wurden.Kosten der Behebung von Fehlern im RM steigen mit den einzelnen Entwicklungsphasen expotentiell. Neben diesen Tatsahen ist es wichtig anzumerken, dass die Erhebung und Analyse der Systemanforderungen betragen etwa 10-15%.

**Probleme bei der Anforderungserhebung**

- unklare ielvorstellungen für das System
- hohe Komplexität  der zu losenden Aufgaben
- Kommunikationsprobleme
- sich ständig veränderde Ziele und Anfrderungen
- schlechte Qualität der Anforderungen

(siehe Buch S. 22-23)


### Begriffe _Requirements Engineering_ und _Requirementsmanagement_

**Requirements Engineering**  ist die Disziplin zur systematischen Entwicklung einer
vollständigen, konsistenten und eindeutigen Spezifikation,
in der beschrieben wird, was ein Software-gestütztes
Produkt tun soll, und die als Grundlage für Vereinbarungen
zwischen allen Betroffenen dient

Durch **Requirements-Engineering** werden dem Analytiker Techniken und Richtlinien an die
Hand gegeben, mit denen Anforderungen und die dazu
gehörigen Informationen verwaltet werden können.

Durch **Requirements-Management** wird ein definiertes Verfahren etabliert, das es ermöglicht,
die oftmals unüberschaubare Anzahl an Anforderungen
komplexer Projekte zu beherrschen.
Anforderungsänderungen werden erst durch die
Einführung von Requirements-Management-Techniken
systematisch handhabbar.

 
## Kapitel 2: Requirements Engineering im Softwareentwicklungsprozess

### Einbettung ins Wasserfallmodell

- Als ein Schritt in dem ganzen Projektplan

##### Vorteile
- Leichtverständliches Modell
- Dokumentieren der Phasenergebnisse
- Eigene Phasen für Planung, Erhebung, Entwurf und Tests

#####Nachteile
- Kein Anforderungsmaangement
- Früheres Festschreiben des Anforderungsdokuments
- Verschieben der Projektrisiken in die Zukunft

### Einbettung in den Unified Process

- anwendungsfallgetrieben
- architekturzentriert
- iterativ und inkrementell

##### Phasen

**Konzeptualisierung (Inception)**

- Beschreibung zu unterstützenden Geschäftsvorfälle
- Definition (Spezifikation) des Endprodukts
- Definition des Produktumfangs

**Entwurf (Elaboration)**

- Planung der Aktivitäten und Ressourcen zur Softwareerstellung
- Festlegung der zu realisierenden Software Eigenschaften
- Festlegen der Software Architektur

**Konstruktion (Construction)**

- Entwicklung des fertigen Produkts
- Validierung des Produkts

**Übergang (Transition)**

- Übergabe des Produkts an den Auftraggeber

> NB! Jede Phase zerfällt in Iterationen, in jeder Iteration werden verschiedene Workflows in unterschiedlicher Intensität earbeitet

##### Workflows

- Geschäftsprozessmodellierung
- Anforderung
- Analyse und Design
- Implementierung
- Test
- Verteilung

> NB! 
> 
> - Workflows inerhalb einer Iteration werden nacheinander bearbeitet
> - In unterschiedlichen Iterationen mit unterschiedlicher Intensität
> - Je nach konkreter Problemstellung geignet koordiniert 


### Einbettung im eXtreme Programming

- auf intensive Kommunikation zwischen Kunden und Software-Entwicklern ausgerichtetes inkrementelles und iteratives Vorgehen zur Software-Entwicklung
- zielt auf möglichst einfache Lösungen (KIS: keep it simple) von Teilproblemen, die nach kurzen Iterationen (1-3 Wochen) produktiv einsetzbar sind 

##### Anfoerderungsmanagement
- Planungsspiel
	- Erhebung der Anforderungen durch Anwender und Entwickler
	- Untersuchungsphase
		- Was soll das System tun?
	- Verpflichtungsphase
		- Welche Anforderungen werden in der nächsten Iteration implementiert?
	- Steuerungsphase
		- Plan an neue Erkentnisse anpassen
- Anforderungsdokumente
	- Userstories
	- Akzaptanztests
	

## Kapitel 3: Requirements Engineering und Management Prozess

**Prozesse** sind strukturierte Folgen von Aktivitäten, die
Eingaben in Ausgaben transformieren.

**Requirements-Management Prozesse** sind strukturierte Folgen von Aktivitäten zum Verstehen der Anwendungsdomäne, zum Erheben der Anforderungen an Softwaresysteme, zum Management von Anforderungsänderungen.

### Beteiligte

- sind alle Personen und Vertreter von Organisationen, die von Einsatz und Erstellung des zu erstellenden Softwaresystems betroffen sind
- verfolgen unterschiedliche Ziele durch die Entwicklung des Software-Systems
- verwenden unterschiedliche Sprachen zur Formulierung ihrer Ziele und der daraus abgeleiteten Systemanforderungen

### Aktivitäten

**Aktivität** ist eine Tätigkeit, bei der Mitarbeiter in definierten Rollen
unter Beachtung vorgegebener Regeln, ausgehend von vorliegenden Artefakten,
unter Verwendung von Methoden und Werkzeugen, neue Artefakte erstellen oder vorliegende Artefakt verändern.

### Dokumente

#### Inputs

- Systemdomäne
- Wünsche der Stakeholder
- (Organisations-) Standards
- Allgemene Regeln

#### Output

- Anforderungsdefinition
	- fasst alle fachlichen Anforderungen an ein zu
entwickelndes Softwareprodukt aus Sicht des
Auftraggebers/Anwenders zusammen
	- skizziert Rahmenbedingungen der Systementwicklung

#### Bestandsteile

##### Vision
- Beschreibung der gemeinsamen
Vorstellungen und der Zielsetzung
des Projekts aller Projektbeteiligten

##### Machbarkeitsstudie
- Abschätzung der wirtschaftlichen und technischen
Machbarkeit des Vorhabens

##### Systemumgebung
- Definition der Einsatzumgebung des Systems
- Definition der Systeme (Hardware und Software), mit
denen das System interagiert
- Definition der Schnittstellen zu diesen Systemen

##### Domänenmodell
- Beschreibung zentraler Dinge und deren Zusammenhänge
aus dem Anwendungsbereich,
	- Objektmodelle
	- Klassendiagramme

##### Anwendungsfallmodell
- Beschreibung von Anwendungsszenarien des Systems aus
verschiedenen Perspektiven mittels textueller und visueller
Modellierungssprachen
	• Beschreibung von Szenarien
	• Anwendungsfalldiagramme
	• Sequenzdiagramme
	• Datenfluss-Diagramme

##### Anforderungsliste (vereinbarte Anforderungen)
- funktionale Anforderungen
	• Dienste und Funktionen, die durch das Softwaresystem bereit
gestellt werden sollen
- nicht-funktionale Anforderungen aus Anwender-Sicht
	• Performanz, Dimensionierung, Verfügbarkeit, …
	• Benutzungsschnittstelle
- nicht-funktionale Anforderungen aus Entwickler-Sicht
	• Portabilität, Korrektheit, Wartbarkeit, …
- Anforderungen an den Entwicklungsprozess
- Anforderungen an die Entwicklungsumgebung

##### Prototypen
- Beschreibung/Sammlung der (Wegwerf-)Prototypen zur Unterstützung
der Anforderungserhebung und zur exemplarischen Systemrealisierung

##### Glossar
- Beschreibung und Definition aller Begriffe des Anforderungsdokuments

## Kapitel 4: Anwendungsdomäne verstehen

> Grundverständnis über Einsatzbereich des Softwaresystems herstellen

### Vision erstellen

#### Vision
- umfasst grobe, abstrakte Anforderungsbeschreibung
- begründet Projektvorhaben
- beschreibt erste, abstrakte Lösungsidee
- liefert Überblick über das zu entwickelnde System
- liefert grobe Einbettung des zu entwickelnden Systems in seine
Systemumgebung

### Systemumgebung dokumentieren

> Kontext ermitteln, in den das zu entwickelnde System eingebettet ist

- Geschäftsprozesse beschreiben
	- Verwendungskontext(Einbettung in umfassende Prozesse)
- Interaktionspartner beschreiben
	- Organisationseinheiten
	- Nachbarsysteme/Nachbarkomponenten
- Schnittstellen festlegen
	- Verankerung des Systems in der Systemumgebung

##### Vorgehen zur System-Abgrenzung

- inside-out
	- Systeminterna grob beschreiben
	- Schnittstellen des System zur Umwelt hieraus ableiten
	- NB! erfordert umfangreiches Wissen über Systeminterna in frühem
Entwicklungsstadium

- outside-in
	- Schnittstellen des Systems zur Umwelt festlegen
	- Systeminterna zu den Schnittstellen beschreiben

### Domänemodell erstellen

> Stakeholder und Anwendungsfälle identifizieren

##### Domänenmodell

- skizziert den Systemumfang
- konkretisiert gemeinsames Verstehen des
Anwendungsbereichs (business understanding)
- konkretisiert gemeinsames Verstehen des Systems und
seiner Einbettung in die Umgebung


### Machbarkeitsstudie erstellen

> untersuchen, ob Projekt sinnvoll durchgeführt werden kann

## Kapitel 5: Anforderungen erheben

**Anforderungen** sind zielgerichtete Aussagen über zu erfüllende Eigenschaften oder zu erbringende Leistungen von Produkten, Prozessen oder Personen.

### Anforderungen ermitteln

### Anforderungen dokumentieren

> klare und vollständige Formulierung aller Anforderungen an das Softwaresystem

#### Anforderungsliste

- fasst alle Anforderungen an das Softwaresystem zusammen
- ergänzt das Systemmodell zum Anforderungsdokument

**Probleme**
- vollständig Beschreibung der Anforderungen
	- Bestandteile von Anforderungen	
	- Aspekte von Anforderungen
	- Qualitätskriterien für Anforderungen
- sinnvolle Notation der Anforderungen

#### Ausprägungen (Detaillierungsebenen)
- Ebene 0
	- grobe Systembeschreibung
- Ebene 1
	- Funktionsbeschreibung
- Ebene 2
	- fachliche Beschreibung
- Ebene 3
	- technische Beschreibung
- Ebene 4
	- Komponentenbeschreibung

### Anforderungen validieren

> Anforderungen und Anforderungsdokument auf Einhalten der Qualitätskriterien überprüfen

### Anforderungen verhandeln und verabschieden

